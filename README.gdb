For disk space and bandwith considerations this
module only contains a small part of gdb.

It was necessary to do this since the whole gdb
distribution is ~56M in sice, but I only imported
the relevant parts of it into CVS so I'll only eat
less than 10M in the repository.

To use it, you need to get the gdb-4.18 tarball
first.

Then do the following:

1.) Unpack the tarball, it will create a `gdb-4.18'
    directory

2.) Remove all files from `gdb-4.18/gdb', but keep
    all subdirectories.

3.) Copy the entire contents of the `gdb' directory
    of our checked out copy of this module (including
    all dot-files such as .cvsignore and the `CVS'
    directories) to `gdb-4.18/gdb'.

This means you replace the `gdb' directory from the
tarball with the `gdb' directory of this module, but
keep all subdirectories from the tarball.

4.) Run autoconf and autoheader in the `gdb' directory,
    but not in top-level directory; also do not run any
    of either aclocal or automake in any directory.

June 6, 1999
Martin Baulig
