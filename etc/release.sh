#! /bin/bash

date=`date +'%Y%m%d'`

(cd gdb && autoheader)
(cd gdb && autoconf)

find . -name CVS | xargs -i rm -rf {}
find . -type f -print | sort > ../manifest
for i in `cat ../manifest`
do
  md5sum $i >> gdb.md5
done

cd ..
tar cf gdb-guile-$date.tar gdb-guile-$date

