#!/bin/bash

test $# -ne 2 && {
    echo "To get a working copy of gdb-guile you need two things:"
    echo " "
    echo "1. A checked out copy of gdb from sourceware CVS"
    echo "   (see http://sourceware.cygnus.com/gdb for details)"
    echo " "
    echo "2. A checked out copy of gdb-guile from GNOME CVS"
    echo " "
    echo "Invoke this script from an empty directory."
    echo " "
    echo "    USAGE:  $0  sourceware-cvs-dir gnome-cvs-dir"
    echo " "
    exit 1
}

sourceware_cvs=`cd $1;pwd`
gnome_cvs=`cd $2;pwd`

sourceware_cvs_root=':pserver:anoncvs@anoncvs.cygnus.com:/cvs/gdb'

(test -d $sourceware_cvs \
    && test -d $sourceware_cvs/CVS \
    && test `cat $sourceware_cvs/CVS/Root` = $sourceware_cvs_root \
    && test `cat $sourceware_cvs/CVS/Repository` = '/cvs/gdb/gdb' \
) || {
    echo "$sourceware_cvs"
    echo " "
    echo "does not look like the top-level gdb dir from Sourceware CVS."
    exit 1
}

(test -d $gnome_cvs \
    && test -d $gnome_cvs/CVS \
    && test `cat $gnome_cvs/CVS/Repository` = '/cvs/gnome/gdb-guile' \
) || {
    echo "$gnome_cvs"
    echo " "
    echo "does not look like the top-level gdb-guile dir from GNOME CVS."
    exit 1
}

echo "Copying files from $sourceware_cvs ..."
cp -a $sourceware_cvs/. .
echo "Removing CVS directories ..."
find . -name CVS | xargs -i rm -rf {}
echo "Deleting all files in gdb/ subdirectory ..."
find gdb -type f -mindepth 1 -maxdepth 1 | xargs -i rm -f {}
echo "Copying files from $gnome_cvs ..."
cp -a $gnome_cvs/gdb .
echo "Done."

