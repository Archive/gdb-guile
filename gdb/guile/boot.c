/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdbcmd.h"
#include <setjmp.h>
#include "top.h"
#include "annotate.h"
#include "symfile.h"
#include "objfiles.h"

#include <guile/gh.h>
#include <readline/readline.h>

#include <sys/stat.h>
#include <unistd.h>

#include "global.h"
#include "callbacks.h"
#include "commands.h"

/* The \"guile shell\" command gives you a guile shell. */
static void guile_shell_command PARAMS ((char *, int));

/* The \"guile eval\" command evals some guile code and
 * prints the resulting expression. */
static void guile_eval_command PARAMS ((char *, int));

/* The \"guile file\" command reads and evals a scheme file. */
static void guile_file_command PARAMS ((char *, int));

/* The "guile init" command initializes the guile interface.
 * This is called automatically when you use any of the other
 * "guile " commands and will later be dropped. */
static void guile_init_command PARAMS ((char *, int));

static SCM scm_gdb_sym_args = SCM_UNDEFINED;
static SCM scm_gdb_sym_output = SCM_UNDEFINED;
static SCM scm_gdb_sym_command = SCM_UNDEFINED;
static SCM scm_gdb_dispatcher_proc = SCM_UNDEFINED;
static SCM scm_gdb_call_dispatcher_proc = SCM_UNDEFINED;

/* This is are alists consisting of \"set\" / \"show\" names and the
 * corresponding 'struct cmd_list_element *' structure (which is
 * converted to scheme via gh_long2scm ()). */
static SCM scm_gdb_set_alist = SCM_UNDEFINED;
static SCM scm_gdb_show_alist = SCM_UNDEFINED;

static SCM scm_last_exception = SCM_UNDEFINED;

static SCM scm_load_path = SCM_UNDEFINED;

/* Set to TRUE in the guile_init_command (). */
static int guile_initialized = 0;

/* Chain containing all defined \"set guile\".  */
struct cmd_list_element *setguilelist = NULL;

/* Chain containing all defined \"show guile\".  */
struct cmd_list_element *showguilelist = NULL;

/* Chain containing all defined guile subcommands.  */
struct cmd_list_element *guilelist = NULL;

/* Variables which control the guile interface.
   These variables are given default values at the end of this file.  */
static char *guile_startfile = NULL;

extern char *guile_eval_cmdline_arg;
extern char *guile_file_cmdline_arg;

typedef struct
{
  char *arg;
  void (*cfunc) (char *, int);
} _gdb_call_dispatcher_args;  

void
scm_debug_print (SCM obj)
{
  scm_display (obj, scm_current_output_port ());
  scm_newline (scm_current_output_port ());
  scm_flush (scm_current_output_port ());
}

static int
_real_call_dispatcher (PTR args)
{
  _gdb_call_dispatcher_args *c_args = (_gdb_call_dispatcher_args *) args;

  if (c_args->cfunc)
    c_args->cfunc (c_args->arg, 0);

  return 0;
}

/**
 *
 * real_call_dispatcher:
 * @self: This is a compiled closure; if you call it via scm_apply () you'll
 *        end up in this function (via call_dispatcher () which passes this
 *        parameter down).
 * @func: Schemified representation (via gh_long2scm ()) of the C function
 *        pointer (`void (*cfunc) (char *args, int from_tty)') which should
 *        be called.
 * @args: Schemified representation (via gh_str02scm ()) of the `args'
 *        parameter which should be passed to the C function.
 * @output: Optional scheme port; if bound we set the current output port
 *          to this port. This is used if we call this function via
 *          scm_call_with_output_string ().
 *
 * Description: Calls a gdb function from guile.
 *
 * Returns:
 **/
static void
real_call_dispatcher (self, func, args, output)
     SCM self, func, args, output;
{
  GDB_GUILE_HOOKS hooks;
  GDB_GUILE_PORTS ports;
  _gdb_call_dispatcher_args c_args;
  int len = 0;

  gdb_guile_save_hooks (&hooks);
  gdb_guile_save_ports (&ports);

  c_args.cfunc = (void (*)(char *, int)) gh_scm2long (func);

  if (!SCM_UNBNDP (args) && (args != SCM_BOOL_F))
    c_args.arg = gh_scm2newstr (args, &len);
  else
    c_args.arg = NULL;

#if 0
  if (!SCM_UNBNDP (output))
    scm_set_current_output_port (output);
#endif

  gdb_guile_enter_hooks (&hooks);

  if (!SCM_UNBNDP (scm_last_exception))
    {
      scm_unprotect_object (scm_last_exception);
      scm_last_exception = SCM_UNDEFINED;
    }

  catch_errors (_real_call_dispatcher, (PTR) &c_args, NULL, RETURN_MASK_ALL);

  gdb_guile_restore_hooks (&hooks);
  gdb_guile_restore_ports (&ports);

  if (c_args.arg)
    free (c_args.arg);

  if (!SCM_UNBNDP (scm_last_exception))
    {
      scm_ithrow (SCM_CAR (scm_last_exception), SCM_CDR (scm_last_exception), 0);
      scm_unprotect_object (scm_last_exception);
      scm_last_exception = SCM_UNDEFINED;
    }
}

/**
 *
 * call_dispatcher:
 * @self: This is the compiled closure which is currently being called
 *        via scm_call_with_output_string ().
 * @output: Newly created string port (from scm_call_with_output_string ()).
 *
 * Description: Called as a compiled closure from call_command (); extracts
 *              "args" and "func" back from the cclo and pass it to the
 *              real_call_dispatcher () to call some gdb function.
 *
 * Returns: SCM_UNDEFINED.
 * (scm_call_with_output_string () later returns the output from the called
 *  C function and discards whatever we return here, but we need to return
 *  something reasonable because scm_apply () expects this function to return
 *  a scheme value.)
 **/
static SCM
call_dispatcher (self, output)
     SCM self, output;
{
  SCM args, func;
  SCM callbacks;

  fprintf (stderr, "call_dispatcher: %lx - %lx\n", self, output);

  args = scm_procedure_property (self, scm_gdb_sym_args);
  func = scm_procedure_property (self, scm_gdb_sym_command);

  real_call_dispatcher (self, func, args, output);

  fprintf (stderr, "call_dispatcher_done: %lx - %lx\n", self, output);

  return SCM_UNDEFINED;
}

static SCM
call_command (cfunc, arg)
     void (*cfunc) PARAMS ((char *args, int from_tty));
     char *arg;
{
  SCM cclo;

  cclo = scm_makcclo (scm_gdb_call_dispatcher_proc, 2L);
  if (arg)
    scm_set_procedure_property_x (cclo, scm_gdb_sym_args,
				  gh_str02scm (arg));
  scm_set_procedure_property_x (cclo, scm_gdb_sym_command,
				gh_long2scm ((long) cfunc));

  return scm_call_with_output_string (cclo);
}

static SCM
dispatcher (SCM self, SCM args, SCM output)
{
  SCM name, func, cclo;

  name = scm_procedure_property (self, scm_sym_name);
  func = scm_procedure_property (self, scm_gdb_sym_command);

  cclo = scm_makcclo (scm_gdb_call_dispatcher_proc, 2L);
  scm_set_procedure_property_x (cclo, scm_gdb_sym_command, func);

  if (!SCM_UNBNDP (args) && (args != SCM_BOOL_F))
    scm_set_procedure_property_x (cclo, scm_gdb_sym_args, args);

  if (!SCM_UNBNDP (output) && (output != SCM_BOOL_F))
    scm_set_procedure_property_x (cclo, scm_gdb_sym_output, output);

  return scm_call_with_output_string (cclo);
}

static SCM
set_dispatcher (SCM name, SCM args)
{
  struct cmd_list_element *c;
  SCM sc;

  sc = scm_assq (name, scm_gdb_set_alist);

  if (sc == SCM_BOOL_F)
    return scm_gdb_set_alist;

  c = (struct cmd_list_element *) gh_scm2long (SCM_CDR (sc));

  switch (c->var_type)
    {
    case var_boolean:
      *(int*) c->var = gh_scm2bool (args);
      break;
    case var_zinteger:
      *(int*) c->var = gh_scm2long (args);
      break;
    case var_integer:
      if (args == SCM_BOOL_T)
	*(int*) c->var = INT_MAX;
      else
	*(int*) c->var = gh_scm2long (args);
      break;
    case var_uinteger:
      if (args == SCM_BOOL_T)
	*(unsigned int*) c->var = UINT_MAX;
      else
	*(unsigned int*) c->var = gh_scm2ulong (args);
      break;
    case var_string:
      *(char**) c->var = gh_scm2newstr (args, NULL);
      break;
    case var_filename:
      *(char**) c->var = tilde_expand (gh_scm2newstr (args, NULL));
      break;
    default:
      return SCM_BOOL_F;
    }

  return SCM_LIST3 (name, args, sc);
}

static SCM
show_dispatcher (SCM name)
{
  struct cmd_list_element *c;
  SCM sc;

  if (name == SCM_BOOL_F)
    {
      long i, length = gh_scm2long (scm_length (scm_gdb_show_alist));
      SCM result = SCM_LIST0;

      for (i = 0; i < length; i++) {
	SCM x = scm_list_ref (scm_gdb_show_alist, gh_long2scm (i));
	result = scm_append_x (SCM_LIST2 (result, SCM_LIST1 (SCM_CAR (x))));
      }

      return result;
    }

  sc = scm_assq (name, scm_gdb_show_alist);

  if (sc == SCM_BOOL_F)
    return scm_gdb_show_alist;

  c = (struct cmd_list_element *) gh_scm2long (SCM_CDR (sc));

  if (c->type == not_set_cmd)
    {
      return call_command (c->function.cfunc, NULL);
    }

  if (c->type != show_cmd)
    return SCM_BOOL_F;

  switch (c->var_type)
    {
    case var_boolean:
      return gh_bool2scm (*(int*) c->var);
    case var_zinteger:
      return gh_long2scm (*(int*) c->var);
    case var_integer:
      if (*(int*) c->var == INT_MAX)
	return SCM_BOOL_T;
      else
	return gh_long2scm (*(int*) c->var);
    case var_uinteger:
      if (*(unsigned int*) c->var == UINT_MAX)
	return SCM_BOOL_T;
      else
	return gh_ulong2scm (*(unsigned int*) c->var);
    case var_string:
    case var_filename:
    case var_enum:
      return gh_str02scm (*(char**) c->var);
    default:
      return SCM_LIST1 (gh_long2scm (c->var_type));
    }

  return SCM_BOOL_F;
}

static void
add_set_list (list, prefix)
     struct cmd_list_element *list;
     char *prefix;
{
  struct cmd_list_element *c;

  for (c = list; c; c = c->next)
    {
      SCM key, value;
      char name [BUFSIZ];
      int i;

      if (c->abbrev_flag)
	continue;

      if (c->prefixlist) {
	add_set_list (*c->prefixlist, c->prefixname+5);
	continue;
      }

      if (c->type != set_cmd)
	continue;

      if (prefix)
	strcpy (name, prefix);
      else
	name [0] = '\0';
      strcat (name, c->name);

      for (i = 0; i < strlen (name); i++)
	if (isspace (name [i]))
	  name [i] = '-';

      key = scm_string_to_symbol (gh_str02scm (name));
      value = gh_long2scm ((long) c);

      if (SCM_UNBNDP (scm_gdb_set_alist))
	scm_gdb_set_alist = SCM_LIST1 (scm_cons (key, value));
      else
	scm_gdb_set_alist = scm_assoc_set_x (scm_gdb_set_alist, key, value);
    }
}

static void
add_show_list (list, prefix)
     struct cmd_list_element *list;
     char *prefix;
{
  struct cmd_list_element *c;

  for (c = list; c; c = c->next)
    {
      SCM key, value;
      char name [BUFSIZ];
      int i;

      if (c->abbrev_flag)
	continue;

      if (c->prefixlist) {
	add_show_list (*c->prefixlist, c->prefixname+5);
	continue;
      }

      if (prefix)
	strcpy (name, prefix);
      else
	name [0] = '\0';
      strcat (name, c->name);

      for (i = 0; i < strlen (name); i++)
	if (isspace (name [i]))
	  name [i] = '-';

      key = scm_string_to_symbol (gh_str02scm (name));
      value = gh_long2scm ((long) c);

      if (SCM_UNBNDP (scm_gdb_show_alist))
	scm_gdb_show_alist = SCM_LIST1 (scm_cons (key, value));
      else
	scm_gdb_show_alist = scm_assoc_set_x (scm_gdb_show_alist, key, value);
    }
}

void
scm_init_gdb_gdb_module ()
{
  struct cmd_list_element *c;
  SCM symcell, cclo;

  scm_init_gdb_gdb_module_stack ();
  scm_init_gdb_gdb_module_commands ();

  scm_load_path = scm_protect_object (scm_intern0 ("%load-path"));
  scm_append_x (SCM_LIST2 (SCM_CDR (scm_load_path),
			   SCM_LIST1 (scm_makfrom0str (GDB_GUILE_SCMDIR))));

  scm_gdb_sym_args = SCM_CAR (scm_protect_object (scm_intern0 ("gdb-args")));
  scm_gdb_sym_output = SCM_CAR (scm_protect_object (scm_intern0 ("gdb-output")));
  scm_gdb_sym_command = SCM_CAR (scm_protect_object (scm_intern0 ("gdb-command")));

  scm_gdb_dispatcher_proc = gh_new_procedure ("dispatcher", dispatcher, 1, 2, 0);
  scm_gdb_call_dispatcher_proc = gh_new_procedure ("call_dispatcher",
						   call_dispatcher, 1, 1, 0);
  
  for (c = cmdlist; c; c = c->next)
    {
      char name [BUFSIZ];

      if (c->abbrev_flag || (c->type != not_set_cmd))
	continue;

      sprintf (name, "gdb-%s", c->name);
      symcell = scm_sysintern0 (name);
      if (!SCM_UNBNDP (SCM_CDR (symcell)))
	continue;

      cclo = scm_makcclo (scm_gdb_dispatcher_proc, 2L);
      scm_set_procedure_property_x (cclo, scm_sym_name, SCM_CAR (symcell));
      scm_set_procedure_property_x (cclo, scm_gdb_sym_command,
				    gh_long2scm ((long) c->function.cfunc));
      SCM_SETCDR (symcell, cclo);
    }

  add_set_list (setlist, NULL);
  add_show_list (showlist, NULL);

  /* Protect them from being freed during garbage collection. */
  scm_protect_object (scm_gdb_set_alist);
  scm_protect_object (scm_gdb_show_alist);

  gh_new_procedure ("gdb-set", set_dispatcher, 2, 2, 0);
  gh_new_procedure ("gdb-show", show_dispatcher, 1, 1, 0);
}

static SCM
catch_handler (data, tag, throw_args)
     void *data;
     SCM tag;
     SCM throw_args;
{
  fprintf (stderr, "CATCH: %p - %ld - %ld\n", data, tag, throw_args);
  return scm_cons (tag, throw_args);
}

static SCM
my_readline (prompt)
     SCM prompt;
{
  int len;

  return gh_str02scm (readline (gh_scm2newstr (prompt, &len)));
}

static void
guile_init_command (args, from_tty)
     char *args;
     int from_tty;
{
  struct stat statb;
  int ret;

  dont_repeat ();

  if (guile_initialized)
    return;

  scm_add_feature ("gpl");

  scm_set_program_arguments (0, NULL, "gdb-guile");

  guile_initialized = 1;

  gh_eval_str ("(define-module (guile-user) :use-module (gdb support) :use-module (gdb gdb) :use-module (gdb callbacks))");

  gdb_guile_init_global ();
  gdb_guile_init_stack ();
  gdb_guile_init_callbacks ();
  gdb_guile_init_structs ();

  /* Check whether the guile_startfile really exists before evaling it. */
  ret = stat (guile_startfile, &statb);
  if (!ret)
    gh_eval_file (guile_startfile);

  if (guile_file_cmdline_arg)
    gh_eval_file (guile_file_cmdline_arg);
  if (guile_eval_cmdline_arg)
    gh_eval_str (guile_eval_cmdline_arg);
}

static void
guile_file_command (args, from_tty)
     char *args;
     int from_tty;
{
  dont_repeat ();

  guile_init_command (NULL, 0);
  gh_eval_file (tilde_expand (args));
}

static void
guile_eval_command (args, from_tty)
     char *args;
     int from_tty;
{
  SCM port, result;

  guile_init_command (NULL, 0);

  port = scm_current_output_port ();
  result = gh_eval_str (args);

  if (!SCM_UNBNDP (result) && (result != SCM_UNSPECIFIED))
    {
      scm_display (result, port);
      scm_newline (port);
    }
  
  scm_flush (port);
}

static void
guile_shell_command (args, from_tty)
     char *args;
     int from_tty;
{
  dont_repeat ();

  guile_init_command (NULL, 0);
  gh_eval_str ("(gdb-top-repl)");
}

/* VARARGS */
static NORETURN void ATTR_NORETURN
guile_error (string, args)
     const char *string;
     va_list args;
{
  SCM errsym, str;
  char *linebuffer;

  errsym = SCM_CAR (scm_intern ("gdb-error", 9));

  vasprintf (&linebuffer, string, args);
  if (linebuffer == NULL)
    str = scm_makfrom0str ("virtual memory exhausted.");
  else
    {
      str = scm_makfrom0str (linebuffer);
      free (linebuffer);
    }

  if (!SCM_UNBNDP (scm_last_exception))
    scm_unprotect_object (scm_last_exception);
  scm_last_exception = scm_protect_object (SCM_LIST2 (errsym, str));

  return_to_top_level (RETURN_ERROR);
}

void
guile_batch_mode (argc, argv)
     int argc;
     char *argv[];
{
  error_hook = guile_error;

  guile_init_command (NULL, 0);
}

void
guile_top_level (argc, argv)
     int argc;
     char *argv[];
{
  error_hook = guile_error;

  if (guile_file_cmdline_arg || guile_eval_cmdline_arg)
    {
      if (!SET_TOP_LEVEL ())
	{
	  guile_init_command (NULL, 0);
	}
    }

  while (1)
    {
      if (!SET_TOP_LEVEL ())
	{
	  do_cleanups (ALL_CLEANUPS);		/* Do complete cleanup */
	  /* GUIs generally have their own command loop, mainloop, or whatever.
	     This is a good place to gain control because many error
	     conditions will end up here via longjmp(). */
	  if (command_loop_hook)
	    command_loop_hook ();
	  else
	    command_loop ();
          quit_command ((char *)0, instream == stdin);
	}
    }
}

/* ARGSUSED */
static void
set_guile (args, from_tty)
     char *args;
     int from_tty;
{
  printf_unfiltered ("\"set guile\" must be followed by the name of a guile subcommand.\n");
  help_list (setguilelist, "set guile ", -1, gdb_stdout);
}

/* ARGSUSED */
static void
show_guile (args, from_tty)
     char *args;
     int from_tty;
{
  cmd_show_list (showguilelist, from_tty, "");
}

/* The "guile" command is defined as a prefix, with allow_unknown = 0.
   Therefore, its own definition is called only for "guile" with no args.  */

/* ARGSUSED */
static void
guile_command (arg, from_tty)
     char *arg;
     int from_tty;
{
  guile_shell_command (NULL, 0);
}

void
initialize_guile ()
{
  char *tmpenv;

  tmpenv = getenv ("GDBGUILESTARTFILE");
  if (tmpenv)
    guile_startfile = savestring (tmpenv, strlen(tmpenv));
  else if (!guile_startfile) {
    guile_startfile = concat (current_directory, "/.gdb_guile", NULL);
  }

  /* Initialize the (gdb gdb) guile module. */
  scm_register_module_xxx ("gdb gdb", scm_init_gdb_gdb_module);

  /* Add generic guile set/show commands. */
  add_prefix_cmd ("guile", class_guile, set_guile,
		  "Generic command for setting guile interface parameters.",
		  &setguilelist, "set guile ", 0, &setlist);
  add_prefix_cmd ("guile", class_guile, show_guile,
		  "Generic command for showing guile interface parameters.",
		  &showguilelist, "show guile ", 0, &showlist);

  /* The guile startfile is read during initialization of the guile
   * interface and can contain arbitrary scheme code. */
  add_show_from_set
    (add_set_cmd ("startfile", no_class, var_filename, (char *)&guile_startfile,
		  "Set the scheme file which is executed on startup.",
		  &setguilelist),
     &showguilelist);

  add_prefix_cmd ("guile", class_guile, guile_command,
		  "Generic command for the guile interface.",
		  &guilelist, "guile ", 0, &cmdlist);

  add_cmd ("init", class_guile, guile_init_command,
	   "Initialize the guile interface.", &guilelist);
  add_cmd ("file", class_guile, guile_file_command,
	   "Reads and evals a scheme file.", &guilelist);
  add_cmd ("shell", class_guile, guile_shell_command,
	   "Gives you a guile shell.", &guilelist);
  add_cmd ("eval", class_guile, guile_eval_command,
	   "Evals a line of scheme code.", &guilelist);
}
