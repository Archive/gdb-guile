/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdbcmd.h"
#include <setjmp.h>
#include "top.h"
#include "annotate.h"

#include <guile/gh.h>
#include <readline/readline.h>

#include "callbacks.h"
#include "global.h"

static SCM scm_gdb_standard_output = SCM_UNDEFINED;
static SCM scm_gdb_error_output = SCM_UNDEFINED;

static SCM scm_gdb_annotate_source = SCM_UNDEFINED;
static SCM scm_gdb_annotate_breakpoint = SCM_UNDEFINED;
static SCM scm_gdb_annotate_catchpoint = SCM_UNDEFINED;
static SCM scm_gdb_annotate_watchpoint = SCM_UNDEFINED;

static SCM scm_gdb_annotate_frames_invalid = SCM_UNDEFINED;

static SCM scm_gdb_annotate_frame_begin = SCM_UNDEFINED;
static SCM scm_gdb_annotate_frame_source = SCM_UNDEFINED;
static SCM scm_gdb_annotate_function_call = SCM_UNDEFINED;
static SCM scm_gdb_annotate_signal_handler_caller = SCM_UNDEFINED;
static SCM scm_gdb_annotate_frame_function_name = SCM_UNDEFINED;
static SCM scm_gdb_annotate_frame_end = SCM_UNDEFINED;

static SCM scm_gdb_quit_confirm = SCM_UNDEFINED;

static int frames_valid = 0;

static void unfiltered_func PARAMS ((const char *, GDB_FILE *));

static void annotate_source_callback PARAMS ((char *, int, int, int, CORE_ADDR));
static void annotate_breakpoint_callback PARAMS ((int));
static void annotate_catchpoint_callback PARAMS ((int));
static void annotate_watchpoint_callback PARAMS ((int));

static void annotate_frames_invalid_callback PARAMS ((void));

static void annotate_frame_begin_callback PARAMS ((int, CORE_ADDR));
static void annotate_frame_source_callback PARAMS ((char *, int, int));
static void annotate_function_call_callback PARAMS ((void));
static void annotate_signal_handler_caller_callback PARAMS ((void));
static void annotate_frame_function_name_callback PARAMS ((char *, enum language));
static void annotate_frame_end_callback PARAMS ((void));

static int quit_confirm_callback PARAMS ((void));

/* Save some gdb hooks so we can modify and later restore them. */
void
gdb_guile_save_hooks (old_hooks)
     GDB_GUILE_HOOKS *old_hooks;
{
  old_hooks->fputs_unfiltered_hook = fputs_unfiltered_hook;
  old_hooks->annotate_source_hook = annotate_source_hook;
  old_hooks->annotate_breakpoint_hook = annotate_breakpoint_hook;
  old_hooks->annotate_catchpoint_hook = annotate_catchpoint_hook;
  old_hooks->annotate_watchpoint_hook = annotate_watchpoint_hook;
  old_hooks->annotate_frames_invalid_hook = annotate_frames_invalid_hook;
  old_hooks->annotate_frame_begin_hook = annotate_frame_begin_hook;
  old_hooks->annotate_frame_source_hook = annotate_frame_source_hook;
  old_hooks->annotate_function_call_hook = annotate_function_call_hook;
  old_hooks->annotate_signal_handler_caller_hook = annotate_signal_handler_caller_hook;
  old_hooks->annotate_frame_function_name_hook = annotate_frame_function_name_hook;
  old_hooks->annotate_frame_end_hook = annotate_frame_end_hook;
  old_hooks->quit_confirm_hook = quit_confirm_hook;
}

/* Restore all gdb hooks which were saved via guile_save_hooks (). */
void
gdb_guile_restore_hooks (old_hooks)
     GDB_GUILE_HOOKS *old_hooks;
{
  fputs_unfiltered_hook = old_hooks->fputs_unfiltered_hook;
  annotate_source_hook = old_hooks->annotate_source_hook;
  annotate_breakpoint_hook = old_hooks->annotate_breakpoint_hook;
  annotate_catchpoint_hook = old_hooks->annotate_catchpoint_hook;
  annotate_watchpoint_hook = old_hooks->annotate_watchpoint_hook;
  annotate_frames_invalid_hook = old_hooks->annotate_frames_invalid_hook;
  annotate_frame_begin_hook = old_hooks->annotate_frame_begin_hook;
  annotate_frame_source_hook = old_hooks->annotate_frame_source_hook;
  annotate_function_call_hook = old_hooks->annotate_function_call_hook;
  annotate_signal_handler_caller_hook = old_hooks->annotate_signal_handler_caller_hook;
  annotate_frame_function_name_hook = old_hooks->annotate_frame_function_name_hook;
  annotate_frame_end_hook = old_hooks->annotate_frame_end_hook;
  quit_confirm_hook = old_hooks->quit_confirm_hook;
}

/* Set all supported callback functions. */
void
gdb_guile_enter_hooks (hooks)
     GDB_GUILE_HOOKS *hooks;
{
  fputs_unfiltered_hook = unfiltered_func;
  annotate_source_hook = annotate_source_callback;
  annotate_breakpoint_hook = annotate_breakpoint_callback;
  annotate_catchpoint_hook = annotate_catchpoint_callback;
  annotate_watchpoint_hook = annotate_watchpoint_callback;
  annotate_frames_invalid_hook = annotate_frames_invalid_callback;
  annotate_frame_begin_hook = annotate_frame_begin_callback;
  annotate_frame_source_hook = annotate_frame_source_callback;
  annotate_function_call_hook = annotate_function_call_callback;
  annotate_signal_handler_caller_hook = annotate_signal_handler_caller_callback;
  annotate_frame_function_name_hook = annotate_frame_function_name_callback;
  annotate_frame_end_hook = annotate_frame_end_callback;
  quit_confirm_hook = quit_confirm_callback;
}

/* Save some scheme ports (at the moment only current output and error
 * port) so we can modify and later restore them. */
void
gdb_guile_save_ports (old_ports)
     GDB_GUILE_PORTS *old_ports;
{
  old_ports->output = scm_current_output_port ();
  old_ports->error = scm_current_error_port ();
}

/* Restore all scheme ports which have been saved via guile_save_port (). */
void
gdb_guile_restore_ports (old_ports)
     GDB_GUILE_PORTS *old_ports;
{
  scm_set_current_output_port (old_ports->output);
  scm_set_current_error_port (old_ports->error);
}

static SCM
callback_dispatcher (SCM symbol, SCM args)
{
  SCM callbacks, callback_hook;
  SCM result;
  int verbose;

  if ((symbol == scm_gdb_standard_output) ||
      (symbol == scm_gdb_error_output))
    verbose = 0;
  else
    verbose = 1;

  if (verbose) {
    fprintf (stderr, "callback_dispatcher: %lx - %lx\n", symbol, args);
  }

  callbacks = scm_apply (scm_gdb_module_ref,
			 SCM_LIST2 (scm_gdb_guile_user_module,
				    gh_symbol2scm ("gdb-callbacks")),
			 SCM_EOL);

  callback_hook = scm_apply (scm_gdb_module_ref,
			     SCM_LIST2 (scm_gdb_gdb_callbacks_module,
					gh_symbol2scm ("gdb-callback-hook")),
			     SCM_EOL);

  result = scm_apply (callback_hook, SCM_LIST2 (symbol, args), SCM_EOL);

  if (verbose)
    scm_debug_print (result);

  return result;
}

/* Called via the 'fputs_unfiltered_hook' to redirect all gdb_output and
 * gdb_error output to the corresponding scheme ports (so we can collect
 * it via scm_call_with_output_string ()).
 *
 * TODO: We need to find some way to make this work for arbitrary GDB_FILE
 *       streams / scheme ports.
 */
static void
unfiltered_func (const char *linebuffer, GDB_FILE *stream)
{
  if (stream == gdb_stdout)
    callback_dispatcher (scm_gdb_standard_output,
			 SCM_LIST1 (gh_str02scm ((char *) linebuffer)));
  else
    callback_dispatcher (scm_gdb_error_output,
			 SCM_LIST1 (gh_str02scm ((char *) linebuffer)));
}

static void
annotate_source_callback (filename, line, character, mid, pc)
     char *filename;
     int line;
     int character;
     int mid;
     CORE_ADDR pc;
{
  callback_dispatcher (scm_gdb_annotate_source,
		       SCM_LIST5 (gh_str02scm (filename),
				  gh_ulong2scm (line),
				  gh_ulong2scm (character),
				  gh_bool2scm (mid),
				  gh_long2scm ((long) pc)));
}

static void
annotate_breakpoint_callback (num)
     int num;
{
  callback_dispatcher (scm_gdb_annotate_breakpoint,
		       SCM_LIST1 (gh_ulong2scm (num)));
}

static void
annotate_catchpoint_callback (num)
     int num;
{
  callback_dispatcher (scm_gdb_annotate_catchpoint,
		       SCM_LIST1 (gh_ulong2scm (num)));
}

static void
annotate_watchpoint_callback (num)
     int num;
{
  callback_dispatcher (scm_gdb_annotate_watchpoint,
		       SCM_LIST1 (gh_ulong2scm (num)));
}

static void
annotate_frames_invalid_callback ()
{
  if (frames_valid)
    callback_dispatcher (scm_gdb_annotate_frames_invalid, SCM_EOL);
  frames_valid = 0;
}

static void
annotate_frame_begin_callback (level, pc)
     int level;
     CORE_ADDR pc;
{
  frames_valid = 1;
  callback_dispatcher (scm_gdb_annotate_frame_begin,
		       SCM_LIST2 (gh_ulong2scm (level),
				  gh_long2scm ((long) pc)));
}

static void
annotate_frame_source_callback (file, line, mid)
     char *file;
     int line, mid;
{
  callback_dispatcher (scm_gdb_annotate_frame_source,
		       SCM_LIST3 (gh_str02scm (file),
				  gh_ulong2scm (line),
				  gh_bool2scm (mid)));
}

static void
annotate_function_call_callback (void)
{
  callback_dispatcher (scm_gdb_annotate_function_call, SCM_EOL);
}

static void
annotate_signal_handler_caller_callback (void)
{
  callback_dispatcher (scm_gdb_annotate_signal_handler_caller, SCM_EOL);
}

static void
annotate_frame_function_name_callback (funname, funlang)
     char *funname;
     enum language funlang;
{
  callback_dispatcher (scm_gdb_annotate_frame_function_name,
		       SCM_LIST2 (gh_str02scm (funname),
				  gh_ulong2scm (funlang)));
}

static void
annotate_frame_end_callback (void)
{
  frames_valid = 1;
  callback_dispatcher (scm_gdb_annotate_frame_end, SCM_EOL);
}

static int
quit_confirm_callback (void)
{
  return gh_scm2bool (callback_dispatcher (scm_gdb_quit_confirm, SCM_EOL));
}

void
gdb_guile_init_callbacks (void)
{
  SCM vcell;

  scm_gdb_standard_output = gh_symbol2scm ("standard-output");
  scm_gdb_error_output = gh_symbol2scm ("error-output");
  scm_gdb_annotate_source = gh_symbol2scm ("annotate-source");
  scm_gdb_annotate_breakpoint = gh_symbol2scm ("annotate-breakpoint");
  scm_gdb_annotate_catchpoint = gh_symbol2scm ("annotate-catchpoint");
  scm_gdb_annotate_watchpoint = gh_symbol2scm ("annotate-watchpoint");
  scm_gdb_annotate_frames_invalid = gh_symbol2scm ("annotate-frames-invalid");
  scm_gdb_annotate_frame_begin = gh_symbol2scm ("annotate-frame-begin");
  scm_gdb_annotate_frame_source = gh_symbol2scm ("annotate-frame-source");
  scm_gdb_annotate_function_call = gh_symbol2scm ("annotate-functi  register char *funname = 0;
  enum language funlang = language_unknown;
on-call");
  scm_gdb_annotate_signal_handler_caller = gh_symbol2scm ("annotate-signal-handler-caller");
  scm_gdb_annotate_frame_function_name = gh_symbol2scm ("annotate-frame-function-name");
  scm_gdb_annotate_frame_end = gh_symbol2scm ("annotate-frame-end");
  scm_gdb_quit_confirm = gh_symbol2scm ("quit-confirm");

  /* [FIXME:] Is this really necessary ? */
  scm_protect_object (scm_gdb_standard_output);
  scm_protect_object (scm_gdb_error_output);
  scm_protect_object (scm_gdb_annotate_source);
  scm_protect_object (scm_gdb_annotate_breakpoint);
  scm_protect_object (scm_gdb_annotate_catchpoint);
  scm_protect_object (scm_gdb_annotate_watchpoint);
  scm_protect_object (scm_gdb_annotate_frames_invalid);
  scm_protect_object (scm_gdb_annotate_frame_begin);
  scm_protect_object (scm_gdb_annotate_frame_source);
  scm_protect_object (scm_gdb_annotate_function_call);
  scm_protect_object (scm_gdb_annotate_signal_handler_caller);
  scm_protect_object (scm_gdb_annotate_frame_function_name);
  scm_protect_object (scm_gdb_annotate_frame_end);
  scm_protect_object (scm_gdb_quit_confirm);
}
