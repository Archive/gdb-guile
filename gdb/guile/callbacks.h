/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef GDB_GUILE_CALLBACKS_H
#define GDB_GUILE_CALLBACKS_H 1

/* Used to save some gdb hooks which are for instance modified in the
 * real_call_dispatcher () before a gdb function is called and then
 * restored before we return to guile. */
typedef struct gdb_guile_hooks
{
  void (*fputs_unfiltered_hook) PARAMS ((const char *, GDB_FILE *));
  void (*annotate_source_hook) PARAMS ((char *, int, int, int, CORE_ADDR));
  void (*annotate_breakpoint_hook) PARAMS ((int));
  void (*annotate_catchpoint_hook) PARAMS ((int));
  void (*annotate_watchpoint_hook) PARAMS ((int));
  void (*annotate_frames_invalid_hook) PARAMS ((void));
  void (*annotate_frame_begin_hook) PARAMS ((int, CORE_ADDR));
  void (*annotate_frame_source_hook) PARAMS ((char *, int, int));
  void (*annotate_function_call_hook) PARAMS ((void));
  void (*annotate_signal_handler_caller_hook) PARAMS ((void));
  void (*annotate_frame_function_name_hook) PARAMS ((char *, enum language));
  void (*annotate_frame_end_hook) PARAMS ((void));
  int (*quit_confirm_hook) PARAMS ((void));
} GDB_GUILE_HOOKS;

/* This is used to save the scm_current_output_port () and the
 * scm_current_error_port () when we scm_call_with_output_string ()
 * some guile function. */
typedef struct gdb_guile_ports
{
  SCM output, error;
} GDB_GUILE_PORTS;

#endif
