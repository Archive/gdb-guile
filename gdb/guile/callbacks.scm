(define-module (gdb callbacks)
  :use-module (gdb structs))

(define-public gdb-current-standard-output '(""))
(define-public gdb-current-error-output '(""))

(define-public gdb-current-frame #f)
(define-public gdb-frames '())

(define-public (gdb-annotate-frame-begin level pc)
  (let* ((this-frame ((record-constructor gdb-frame-rec
					  '(level pc))
		      level pc)))
    (set! gdb-current-frame this-frame)))

(define-public (gdb-annotate-frame-end)
  (if (record? gdb-current-frame)
      (let* ((level ((record-accessor gdb-frame-rec 'level)
		     gdb-current-frame)))
	(set! gdb-frames
	      (assq-set! gdb-frames level gdb-current-frame)))))

(define-public (gdb-annotate-frame-function-name funname funlang)
  (if (record? gdb-current-frame)
      (let* ((funname-write! (record-modifier gdb-frame-rec 'function))
	     (funlang-write! (record-modifier gdb-frame-rec 'language)))
	(funname-write! gdb-current-frame funname)
	(funlang-write! gdb-current-frame funlang))))

(define-public (gdb-annotate-frame-source file line mid)
  (if (record? gdb-current-frame)
      (let* ((file-write! (record-modifier gdb-frame-rec 'file))
	     (line-write! (record-modifier gdb-frame-rec 'line))
	     (mid-write! (record-modifier gdb-frame-rec 'mid)))
	(file-write! gdb-current-frame file)
	(line-write! gdb-current-frame line)
	(mid-write! gdb-current-frame mid))))

(define-public (gdb-annotate-frames-invalid)
  (set! gdb-current-frame #f)
  (set! gdb-frames '()))


(define-public gdb-default-callbacks
  '((annotate-source . (lambda (file line char mid pc)
			 (list 'annotate-source
			       file line char mid pc))
		     )

    (annotate-breakpoint . (lambda (number)
			     (list 'annotate-breakpoint
				   number))
			 )

    (annotate-catchpoint . (lambda (number)
			     (list 'annotate-catchpoint
				   number))
			 )

    (annotate-watchpoint . (lambda (number)
			     (list 'annotate-watchpoint
				   number))
			 )

    (annotate-frames-invalid . (lambda ()
				 (gdb-annotate-frames-invalid)
				 (list 'annotate-frames-invalid)
				 )
			     )

    (annotate-frame-begin . (lambda (level pc)
			      (gdb-annotate-frame-begin level pc)
			      (list 'annotate-frame-begin level pc)
			      )
			  )

    (annotate-frame-source . (lambda (file line mid)
			       (gdb-annotate-frame-source file line mid)
			       (list 'annotate-frame-source file line mid)
			       )
			   )

    (annotate-function-call . (lambda ()
				(list 'annotate-function-call))
			    )

    (annotate-signal-handler-caller . (lambda ()
					(list 'annotate-signal-handler-caller))
				    )

    (annotate-frame-function-name . (lambda (funname funlang)
				      (gdb-annotate-frame-function-name
				       funname funlang)
				      (list 'annotate-frame-function-name
					    funname funlang)
				      )
				  )

    (annotate-frame-end . (lambda ()
			    (gdb-annotate-frame-end)
			    (list 'annotate-frame-end)
			    )
			)

    (standard-output . (lambda (output)
			 (set-car! gdb-current-standard-output
				   (string-append
				    (car gdb-current-standard-output)
				    output))
			 )
		     )

    (error-output . (lambda (output)
		      (set! gdb-current-error-output
			    (string-append
			     (car gdb-current-error-output)
			     output))
		      )
		  )

    (quit-confirm . (lambda () #t))

    (exception-handler . (lambda (error error-args . args)
			   (list 'exception-handler
				 error error-args (car args)))
		       )
    )
  )

(define-public gdb-callbacks #t)
(define-public gdb-old-callbacks #t)

(defmacro-public gdb-call-with-callback-vector (cbv expr)
  `(let ((old-callbacks #f))

    (dynamic-wind

     ;; called at entry.
     (lambda ()
       (set! gdb-old-callbacks (cons gdb-old-callbacks gdb-callbacks))
       (set! old-callbacks gdb-callbacks)
       (set! gdb-callbacks ,cbv))

     ;; the protected thunk.
     (lambda () ,expr)

     ;; called at exit.
     (lambda ()
       (set! gdb-old-callbacks (car gdb-old-callbacks))
       (set! gdb-callbacks old-callbacks)))))

(define debug-print-flag #t)

(define debug-print
  (lambda (name value)
    (if debug-print-flag
	(begin
	  (display (list name value)) (newline)
	  (flush-all-ports)))))

(define find-callback-func
  (lambda (callback-vector old-vector callback)
    (let* ((get-inherit-flag (lambda (cbv)
			       (if (boolean? cbv) cbv (car cbv)))
			     )
	   (get-callback-acons (lambda (cbv)
				 (if (boolean? cbv) '() (cdr cbv)))
			       )
	   (callback-acons (get-callback-acons callback-vector))
	   (callback-func (assoc-ref callback-acons callback))
	   )
      ;; Check whether the callback function can be found in the
      ;; current callback vector.
      (if (and (boolean? callback-func) (not callback-func))
	  ;; If not, we first need to check whether we inherit from
	  ;; any other callback vector.
	  (if (get-inherit-flag callback-vector)
	      ;; We inherit from old-vector ...
	      (begin
		(if (boolean? old-vector)
		    (if old-vector
			;; if old-vector is #t, we inherit from
			;; gdb-default-callbacks
			(find-callback-func (cons #f gdb-default-callbacks)
					    #f callback)
			;; if it's #f, then there's no vector left we can
			;; inherit from, so we need to return #f.
			#f)
		    ;; If old-vector is not a boolean, then we've been
		    ;; called via gdb-call-with-callback-vector in which
		    ;; case the cdr is the old value of gdb-callbacks
		    ;; and its car the old value of gdb-old-callbacks.
		    (find-callback-func (cdr old-vector) (car old-vector)
					callback)))
	      ;; The callback function was not found and we also have
	      ;; no other callback vector to inherit from; so we need
	      ;; to return #f here.
	      #f)
	  ;; The callback function was found in the current callback
	  ;; vector, so we return it.
	  callback-func))))

;;
;; (gdb-callback-hook callback-name callback-args)
;;
;; This is called from GDB for all callbacks.
;;

(define-public (gdb-callback-hook callback-name callback-args)
  (letrec (;; This is an exception handler which we use when we call
	   ;; call-the-hook first.
	   ;;
	   ;; If the exception occured because you specified a wrong
	   ;; callback function (wrong-number-of-args or wrong-type-arg),
	   ;; then this will simply return #f, otherwise it'll try to
	   ;; call a callback named `exception-handler' which you can set
	   ;; to a (lambda (error error-args . args)) function: `error'
	   ;; and `error-args' are the arguments from the exception and
	   ;; `args' are the callback-args.
	   ;;
	   ;; While we call that `exception-handler' callback, all further
	   ;; exceptions will be ignored.

	   (exception-handler
	    (lambda (error . args)
	      ;; For any of these exceptions we simply return #f because
	      ;; the user gave us an incorrect callback function.
	      (let ((ignore-list (list 'wrong-number-of-args
				       'wrong-type-arg)))
		(if (memq error ignore-list) #f
		    ;; This happens when there's any exception during the
		    ;; callback. You can provide your own `exception-handler'
		    ;; callback which will be called in this case.
		    ;;
		    ;; Whatever happens here: we ignore all further exceptions
		    ;; and return #f.
		    ;;
		    ;; If there is no `exception-handler' callback,
		    ;; call-the-hook will return #f itself.
		    (false-if-exception
		     (call-the-hook 'exception-handler
				    (list error args callback-args)))))))

	   ;; This will really call the callback hook.
	   ;; We call it with two arguments: the name of the callback and
	   ;; a list of all arguments which we want to pass to that callback.
	   
	   (call-the-hook
	    (lambda (callback args)
	      (let* ((callback-func (find-callback-func gdb-callbacks
							gdb-old-callbacks
							callback))
		     ;; find-callback-func may return #f so we need to check
		     ;; whether we can really call its return value as a
		     ;; procedure first.
		     (can-call-it (and (not (boolean? callback-func))
				       (procedure? (eval callback-func))))
		     )
		(if can-call-it
		    (apply (eval callback-func) args)
		    #f))))
	   )

    ;; Main part of `gdb-callback-hook'.
    ;;
    ;; We simply call `call-the-hook' here with `exception-handler'
    ;; for all exceptions.

    (catch #t
	   (lambda ()
	     (call-the-hook callback-name callback-args))
	   exception-handler)))
