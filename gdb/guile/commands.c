/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdbcmd.h"
#include <setjmp.h>
#include "top.h"
#include "annotate.h"
#include "symfile.h"
#include "objfiles.h"

#include <guile/gh.h>

#include <sys/stat.h>
#include <unistd.h>

#include "commands.h"

/* The "No symbol table loaded" message from symtab.c */
extern char* no_symtab_msg;

static SCM
get_sources ()
{

  SCM source_list;
  SCM source;
  SCM already_exists;

  register struct symtab *s;
  register struct partial_symtab *ps;
  register struct objfile *objfile;

  source_list = SCM_EOL;

  if (!have_full_symbols () && !have_partial_symbols ()) 
    {
      error (no_symtab_msg);
    }

  ALL_SYMTABS (objfile, s) 
    {
      source = gh_str2scm (s->filename, strlen (s->filename));
      source_list = scm_cons (source, source_list);
    }
     
  ALL_PSYMTABS (objfile, ps) 
    {
      if (!ps->readin) 
        {
          /* FIXME: Deal with the multiple-partial-symtab-entries problem
             described in symtab.c:3306 */
          source = gh_str2scm (ps->filename, strlen (ps->filename));
          source_list = scm_cons (source, source_list);          
        }
    }


  return source_list;
}

static SCM
scm_execute_command (SCM command)
{
  execute_command (gh_scm2newstr (command, NULL), 0);

  return SCM_BOOL_F;
}

void
scm_init_gdb_gdb_module_commands ()
{
  gh_new_procedure ("gdb-sources", get_sources, 0, 0, 0);
  gh_new_procedure ("gdb-execute", scm_execute_command, 1, 0, 0);
}

