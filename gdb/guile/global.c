/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdbcmd.h"
#include <setjmp.h>
#include "top.h"
#include "annotate.h"

#include <guile/gh.h>
#include <readline/readline.h>

SCM scm_gdb_module_ref;
SCM scm_gdb_module_set_x;

SCM scm_gdb_gdb_structs;
SCM scm_gdb_gdb_structs_module;
SCM scm_gdb_gdb_callbacks;
SCM scm_gdb_gdb_callbacks_module;

SCM scm_gdb_guile_user;
SCM scm_gdb_guile_user_module;

SCM scm_gdb_gdb_frame_rec;
SCM scm_gdb_gdb_frame_fields;

SCM scm_gdb_list_index;

SCM scm_sym_gdb_error;

void
gdb_guile_init_global (void)
{
  scm_gdb_module_ref = SCM_CDR (scm_intern0 ("module-ref"));
  scm_gdb_module_set_x = SCM_CDR (scm_intern0 ("module-set!"));

  scm_gdb_guile_user = SCM_LIST1 (gh_symbol2scm ("guile-user"));
  scm_gdb_guile_user_module = scm_resolve_module (scm_gdb_guile_user);

  scm_gdb_gdb_structs = SCM_LIST2 (gh_symbol2scm ("gdb"),
				   gh_symbol2scm ("structs"));
  scm_gdb_gdb_structs_module = scm_resolve_module (scm_gdb_gdb_structs);

  scm_gdb_gdb_callbacks = SCM_LIST2 (gh_symbol2scm ("gdb"),
				     gh_symbol2scm ("callbacks"));
  scm_gdb_gdb_callbacks_module = scm_resolve_module (scm_gdb_gdb_callbacks);

  scm_gdb_gdb_frame_rec = scm_apply (scm_gdb_module_ref,
				     SCM_LIST2 (scm_gdb_gdb_structs_module,
						gh_symbol2scm ("gdb-frame-rec")),
				     SCM_EOL);

  scm_gdb_gdb_frame_fields = scm_apply (scm_gdb_module_ref,
					SCM_LIST2 (scm_gdb_gdb_structs_module,
						   gh_symbol2scm ("gdb-frame-fields")),
					SCM_EOL);

  scm_gdb_list_index = scm_apply (scm_gdb_module_ref,
				  SCM_LIST2 (scm_gdb_guile_user_module,
					     gh_symbol2scm ("list-index")),
				  SCM_EOL);

  scm_sym_gdb_error = SCM_CAR (scm_protect_object (scm_intern0 ("gdb-error")));

  scm_protect_object (scm_gdb_module_ref);
  scm_protect_object (scm_gdb_module_set_x);
  scm_protect_object (scm_gdb_guile_user);
  scm_protect_object (scm_gdb_guile_user_module);
  scm_protect_object (scm_gdb_gdb_structs);
  scm_protect_object (scm_gdb_gdb_structs_module);
  scm_protect_object (scm_gdb_gdb_callbacks);
  scm_protect_object (scm_gdb_gdb_callbacks_module);

  scm_protect_object (scm_gdb_gdb_frame_rec);
  scm_protect_object (scm_gdb_gdb_frame_fields);
  scm_protect_object (scm_gdb_list_index);
}
