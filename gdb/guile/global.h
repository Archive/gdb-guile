/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#ifndef GDB_GUILE_GLOBAL_H
#define GDB_GUILE_GLOBAL_H 1

extern SCM scm_gdb_module_ref;
extern SCM scm_gdb_module_set_x;

extern SCM scm_gdb_gdb_structs;
extern SCM scm_gdb_gdb_structs_module;
extern SCM scm_gdb_gdb_callbacks;
extern SCM scm_gdb_gdb_callbacks_module;
extern SCM scm_gdb_guile_user;
extern SCM scm_gdb_guile_user_module;

extern SCM scm_gdb_gdb_frame_rec;
extern SCM scm_gdb_gdb_frame_fields;

extern SCM scm_gdb_list_index;

extern SCM scm_sym_gdb_error;

extern void gdb_guile_init_global (void);

#endif
