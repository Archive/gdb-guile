/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include <ctype.h>
#include "defs.h"
#include "gdb_string.h"
#include "value.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "expression.h"
#include "language.h"
#include "frame.h"
#include "gdbcmd.h"
#include "gdbcore.h"
#include "target.h"
#include "breakpoint.h"
#include "demangle.h"
#include "inferior.h"
#include "annotate.h"
#include "symfile.h"
#include "objfiles.h"

#include <guile/gh.h>
#include <libguile/snarf.h>
#include <readline/readline.h>

#include "global.h"
#include "structs.h"

static gdb_frame_rec_itable frame_rec_itable;

static SCM scm_sym_called_from_gdb;
static SCM scm_sym_signal_handler_caller;
static SCM scm_sym_no_stack;

static SCM scm_gdb_frame_command (void);
static SCM scm_gdb_backtrace_command (void);

static SCM
print_frame_info_base (struct frame_info *fi, int level)
{
  struct symtab_and_line sal;
  struct symbol *func;
  register char *funname = 0;
  enum language funlang = language_unknown;
  SCM frame;

  frame = scm_make_struct (scm_gdb_gdb_frame_rec, SCM_MAKINUM (0), SCM_EOL);

  if (frame_in_dummy (fi))
    {
      scm_struct_set_x (frame, frame_rec_itable.type,
			scm_sym_called_from_gdb);
      scm_struct_set_x (frame, frame_rec_itable.level,
			SCM_MAKINUM (level == -1 ? 0 : level));
      scm_struct_set_x (frame, frame_rec_itable.pc,
			SCM_MAKINUM (fi->pc));
      return frame;
    }
  if (fi->signal_handler_caller)
    {
      scm_struct_set_x (frame, frame_rec_itable.type,
			scm_sym_signal_handler_caller);
      scm_struct_set_x (frame, frame_rec_itable.level,
			SCM_MAKINUM (level == -1 ? 0 : level));
      scm_struct_set_x (frame, frame_rec_itable.pc,
			SCM_MAKINUM (fi->pc));
      return frame;
    }

  /* If fi is not the innermost frame, that normally means that fi->pc
     points to *after* the call instruction, and we want to get the line
     containing the call, never the next line.  But if the next frame is
     a signal_handler_caller or a dummy frame, then the next frame was
     not entered as the result of a call, and we want to get the line
     containing fi->pc.  */
  sal =
    find_pc_line (fi->pc,
		  fi->next != NULL
		  && !fi->next->signal_handler_caller
		  && !frame_in_dummy (fi->next));

  func = find_pc_function (fi->pc);
  if (func)
    {
      /* In certain pathological cases, the symtabs give the wrong
	 function (when we are in the first function in a file which
	 is compiled without debugging symbols, the previous function
	 is compiled with debugging symbols, and the "foo.o" symbol
	 that is supposed to tell us where the file with debugging symbols
	 ends has been truncated by ar because it is longer than 15
	 characters).  This also occurs if the user uses asm() to create
	 a function but not stabs for it (in a file compiled -g).

	 So look in the minimal symbol tables as well, and if it comes
	 up with a larger address for the function use that instead.
	 I don't think this can ever cause any problems; there shouldn't
	 be any minimal symbols in the middle of a function; if this is
	 ever changed many parts of GDB will need to be changed (and we'll
	 create a find_pc_minimal_function or some such).  */

      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);
      if (msymbol != NULL
	  && (SYMBOL_VALUE_ADDRESS (msymbol) 
	      > BLOCK_START (SYMBOL_BLOCK_VALUE (func))))
	{
#if 0
	  /* There is no particular reason to think the line number
	     information is wrong.  Someone might have just put in
	     a label with asm() but left the line numbers alone.  */
	  /* In this case we have no way of knowing the source file
	     and line number, so don't print them.  */
	  sal.symtab = 0;
#endif
	  /* We also don't know anything about the function besides
	     its address and name.  */
	  func = 0;
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	}
      else
	{
          /* I'd like to use SYMBOL_SOURCE_NAME() here, to display
           * the demangled name that we already have stored in
           * the symbol table, but we stored a version with
           * DMGL_PARAMS turned on, and here we don't want
           * to display parameters. So call the demangler again,
           * with DMGL_ANSI only. RT
           * (Yes, I know that printf_symbol_filtered() will
           * again try to demangle the name on the fly, but
           * the issue is that if cplus_demangle() fails here,
           * it'll fail there too. So we want to catch the failure
           * ("demangled==NULL" case below) here, while we still
           * have our hands on the function symbol.)
           */
          char * demangled;
	  funname = SYMBOL_NAME (func);
	  funlang = SYMBOL_LANGUAGE (func);
          if (funlang == language_cplus) {
            demangled = cplus_demangle (funname, DMGL_ANSI);
            if (demangled == NULL)
              /* If the demangler fails, try the demangled name
               * from the symbol table. This'll have parameters,
               * but that's preferable to diplaying a mangled name.
               */
	      funname = SYMBOL_SOURCE_NAME (func);
          }
	}
    }
  else
    {
      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);
      if (msymbol != NULL)
	{
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	}
    }

  scm_struct_set_x (frame, frame_rec_itable.level,
		    SCM_MAKINUM (level == -1 ? 0 : level));
  scm_struct_set_x (frame, frame_rec_itable.pc,
		    SCM_MAKINUM (fi->pc));
  scm_struct_set_x (frame, frame_rec_itable.function,
		    scm_makfrom0str (funname ? funname : "??"));
  scm_struct_set_x (frame, frame_rec_itable.language,
		    scm_assq_ref (scm_gdb_languages_alist_2,
				  SCM_MAKINUM (funlang)));

  if (sal.symtab && sal.symtab->filename)
    {
      scm_struct_set_x (frame, frame_rec_itable.file,
			scm_makfrom0str (sal.symtab->filename));
      scm_struct_set_x (frame, frame_rec_itable.mid,
			gh_bool2scm (fi->pc != sal.pc));
      scm_struct_set_x (frame, frame_rec_itable.line,
			SCM_MAKINUM (sal.line));
    }

  return frame;
}

SCM_PROC (s_frame_command, "gdb-frame", 0, 0, 0, scm_gdb_frame_command);

static SCM
scm_gdb_frame_command (void)
{
  if (!target_has_stack)
    scm_throw (scm_sym_gdb_error, SCM_LIST1 (scm_sym_no_stack));

  return print_frame_info_base (selected_frame, selected_frame_level);
}

SCM_PROC (s_backtrace_command, "gdb-backtrace", 0, 0, 0, scm_gdb_backtrace_command);

static SCM
scm_gdb_backtrace_command (void)
{
  struct frame_info *fi;
  struct partial_symtab *ps;
  register struct frame_info *trailing;
  register int trailing_level, i;
  SCM backtrace = SCM_LIST0;

  if (!target_has_stack)
    scm_throw (scm_sym_gdb_error, SCM_LIST1 (scm_sym_no_stack));

  trailing = get_current_frame ();
  trailing_level = 0;

  /* Read in symbols for all of the frames.  Need to do this in
     a separate pass so that "Reading in symbols for xxx" messages
     don't screw up the appearance of the backtrace.  Also
     if people have strong opinions against reading symbols for
     backtrace this may have to be an option.  */
  for (fi = trailing; fi != NULL; fi = get_prev_frame (fi))
    {
      QUIT;
      ps = find_pc_psymtab (fi->pc);
      if (ps)
	PSYMTAB_TO_SYMTAB (ps);	/* Force syms to come in */
    }

  for (i = 0, fi = trailing; fi != NULL; i++, fi = get_prev_frame (fi))
    {
      SCM x;

      QUIT;

      x = print_frame_info_base (trailing, trailing_level + i);
      backtrace = scm_append_x (SCM_LIST2 (backtrace, SCM_LIST1 (x)));
    }

  return backtrace;
}

void
gdb_guile_init_stack (void)
{
  memset (&frame_rec_itable, 0, sizeof (gdb_frame_rec_itable));

  frame_rec_itable.type = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("type")),
		SCM_EOL));
  frame_rec_itable.level = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("level")),
		SCM_EOL));
  frame_rec_itable.file = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("file")),
		SCM_EOL));
  frame_rec_itable.line = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("line")),
		SCM_EOL));
  frame_rec_itable.mid = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("mid")),
		SCM_EOL));
  frame_rec_itable.pc = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("pc")),
		SCM_EOL));
  frame_rec_itable.function = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("function")),
		SCM_EOL));
  frame_rec_itable.language = scm_permanent_object
    (scm_apply (scm_gdb_list_index,
		SCM_LIST2 (scm_gdb_gdb_frame_fields, gh_symbol2scm ("language")),
		SCM_EOL));

  scm_sym_called_from_gdb = scm_permanent_object (SCM_CAR (scm_intern0 ("called-from-gdb")));
  scm_sym_signal_handler_caller = scm_permanent_object (SCM_CAR (scm_intern0 ("signal-handler-caller")));
  
  scm_sym_no_stack = scm_permanent_object (SCM_CAR (scm_intern0 ("no-stack")));
}

void
scm_init_gdb_gdb_module_stack ()
{
#ifndef SCM_MAGIC_SNARFER
#include "stack.x"
#endif
}
