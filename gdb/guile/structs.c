/* Guile interface for GDB.
   Copyright 1999 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdbcmd.h"
#include <setjmp.h>
#include "top.h"
#include "annotate.h"

#include <guile/gh.h>
#include <readline/readline.h>

#include "global.h"
#include "structs.h"

SCM scm_gdb_languages;
SCM scm_gdb_languages_alist;
SCM scm_gdb_languages_alist_2;

static void
add_language (lang, enumval)
     char *lang;
     int enumval;
{
  SCM key, value;

  key = gh_str02scm (lang);
  value = gh_ulong2scm (enumval);
 
  if (SCM_UNBNDP (scm_gdb_languages_alist))
    scm_gdb_languages_alist = SCM_LIST1 (scm_cons (key, value));
  else
    scm_gdb_languages_alist = scm_assoc_set_x (scm_gdb_languages_alist,
					       key, value);

  if (SCM_UNBNDP (scm_gdb_languages_alist_2))
    scm_gdb_languages_alist_2 = SCM_LIST1 (scm_cons (value, key));
  else
    scm_gdb_languages_alist_2 = scm_assoc_set_x (scm_gdb_languages_alist_2,
						 value, key);
}

void
gdb_guile_init_structs (void)
{
  scm_gdb_languages = gh_symbol2scm ("gdb-languages");
  scm_gdb_languages_alist = SCM_UNDEFINED;
  scm_gdb_languages_alist_2 = SCM_UNDEFINED;

  add_language ("unknown", language_unknown);
  add_language ("auto", language_auto);
  add_language ("c", language_c);
  add_language ("cplus", language_cplus);
  add_language ("java", language_java);
  add_language ("chill", language_chill);
  add_language ("fortran", language_fortran);
  add_language ("m2", language_m2);
  add_language ("asm", language_asm);
  add_language ("scm", language_scm);

  scm_protect_object (scm_gdb_languages);
  scm_protect_object (scm_gdb_languages_alist);
  scm_protect_object (scm_gdb_languages_alist_2);

  scm_apply (scm_gdb_module_set_x,
	     SCM_LIST3 (scm_gdb_gdb_structs_module,
			scm_gdb_languages, scm_gdb_languages_alist),
	     SCM_EOL);
}
