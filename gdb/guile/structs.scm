(define-module (gdb structs))

;; This will be set in gdb_guile_init_structs ().
(define-public gdb-languages #f)

(define-public gdb-frame-fields '(type level file line mid pc function language))
(define-public gdb-frame-rec (make-record-type "gdb-frame-rec"
					       gdb-frame-fields))

