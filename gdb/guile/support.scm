(define-module (gdb support))

;; we need to define our own top-repl here.
(define-public (gdb-top-repl) 

  ;; Load emacs interface support if emacs option is given.
  (if (and (module-defined? the-root-module 'use-emacs-interface)
	   use-emacs-interface)
      (load-emacs-interface))

  ;; Place the user in the guile-user module.
  (define-module (guile-user))

  (let ((old-handlers #f)
	(signals `((,SIGINT . "User interrupt")
		   (,SIGFPE . "Arithmetic error")
		   (,SIGBUS . "Bad memory access (bus error)")
		   (,SIGSEGV . "Bad memory access (Segmentation violation)"))))

    (dynamic-wind

     ;; call at entry
     (lambda ()
       (let ((make-handler (lambda (msg)
			     (lambda (sig)
			       (save-stack %deliver-signals)
			       (scm-error 'signal
					  #f
					  msg
					  #f
					  (list sig))))))
	 (set! old-handlers
	       (map (lambda (sig-msg)
		      (sigaction (car sig-msg)
				 (make-handler (cdr sig-msg))))
		    signals))))

     ;; the protected thunk.
     (lambda ()

       ;; If we've got readline, use it to prompt the user.  This is a
       ;; kludge, but we'll fix it soon.  At least we only get
       ;; readline involved when we're actually running the repl.
       (if (and (memq 'readline *features*)
		(isatty? (current-input-port))
		(not (and (module-defined? the-root-module
					   'use-emacs-interface)
			  use-emacs-interface)))
	   (let ((read-hook (lambda () (run-hook before-read-hook))))
	     (set! old-input-port (current-input-port))
	     (set-current-input-port (readline-port))
	     (set! repl-reader
		   (lambda (prompt)
		     (dynamic-wind
		      (lambda ()
			(set-readline-prompt! prompt)
			(set-readline-read-hook! read-hook))
		      (lambda () (read))
		      (lambda ()
			(set-readline-prompt! "")
			(set-readline-read-hook! #f)))))))
       (let ((status (scm-style-repl)))
	 (run-hook exit-hook)
	 status))

     ;; call at exit.
     (lambda ()
       (map (lambda (sig-msg old-handler)
	      (if (not (car old-handler))
		  ;; restore original C handler.
		  (sigaction (car sig-msg) #f)
		  ;; restore Scheme handler, SIG_IGN or SIG_DFL.
		  (sigaction (car sig-msg)
			     (car old-handler)
			     (cdr old-handler))))
			 signals old-handlers)))))

